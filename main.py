import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
import threading
import sys

from create_satellites import set_satellites
from create_calculate_scenario import set_facilitys
from calculate_handover_strategy import generate_handover_strategy

# 1. set satellites
satellites = set_satellites()

satellite_load_num = [0]*9

# UE类
class UE:
    def __init__(self,id,position,time,satelliteId):
        self.id = id # 用户id
        self.server = satelliteId # 服务卫星
        satellite_load_num[satelliteId] += 1
        self.position = position # 位置
        self.startTime = time
        self.interval = 2*3600
        self.result = [] # 切换窗口
        self.timer = None
        self._callback()
    
    def start_timer(self):
        """启动定时器"""
        self.startTime = self.startTime + timedelta(hours=2) # delta t
        self.timer = threading.Timer(self.interval, self._callback)
        self.timer.start()

    def stop_timer(self):
        """停止定时器"""
        if self.timer:
            self.timer.cancel()

    def _callback(self):
        """定时器回调函数"""
        # 调用外部函数并赋值给类成员
        #self.startTime = self.startTime + timedelta(hours=24)
        stopTime = self.startTime + timedelta(hours=2) # delta t
        #self.result,self.server = generate_handover_strategy(self.startTime,stopTime,self.server,self.position) 
        self.result = self.result + generate_handover_strategy(self.startTime,stopTime,self.server,self.position,satellites,satellite_load_num)
        self.start_timer()


#读取位置信息
ues = []


#输出
def out():
    time_str = datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '.txt'
    sys.stdout = open(time_str,'a')
    for ue in ues:
        for window in ue.result:
            #if window[2] <= datetime.now(): # 在切换窗口
                print(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
                print(ue.id,window,ue.position,ue.server)
                if ue.server != window[1] and window[3] <= datetime.now(): # 切换发生
                    ue.server = window[1]
                    satellite_load_num[window[0]] -= 1
                    satellite_load_num[window[1]] += 1
                    print("###",satellite_load_num)
                    del ue.result[0]
    sys.stdout = sys.__stdout__
                
    timer = threading.Timer(60,out)
    timer.start()


timer = threading.Timer(1,out)
timer.start()

# 尝试创建UE对象
start = time.time()
latitude=36
longtitude=108
for i in range(10000):
    print("创建用户",i)
    ue1 = UE(i,set_facilitys(np.random.uniform(-90, 90), np.random.uniform(-180, 180)),datetime.now().replace(microsecond=0),0)
    ues.append(ue1)
print("success")
end = time.time()
# 计算耗时
elapsed_time = end - start
print(f"运行耗时: {elapsed_time:.6f} 秒")

# 用户位置更新

#for window in ue1.result:
    #if (datetime.now()+timedelta(hours = 6))>window[2]:
        #serverId = window[1]
#ue2 = UE(2,set_facilitys(0, 108),(datetime.now()+timedelta(hours = 6)).replace(microsecond=0),serverId)
#ues.append(ue2)


#for ue in ues:
    #print(ue.position)
    #print(ue.result)