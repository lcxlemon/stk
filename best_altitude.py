import queue
import time
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from skyfield.api import EarthSatellite, Topos, load

# 加载天文常数
ts = load.timescale()

# TSN八星 时刻2025 1 1 4 0 0
line1 = "1 99999U          25001.16666667  .00000063  00000-0  20868+3 0 00005"
line2 = "2 99999 053.2029 199.3647 0002250 270.1895 089.5351 02.00529944000015"
satellite1 = EarthSatellite(line1, line2, "Satellite1", ts)

line1 = "1 99999U          25001.16666667  .00000159  00000-0  52980+3 0 00001"
line2 = "2 99999 053.2584 244.3044 0002257 269.9682 359.8996 02.00529003000014"
satellite2 = EarthSatellite(line1, line2, "Satellite2", ts)

line1 = "1 99999U          25001.16666667  .00000025  00000-0  81914+2 0 00001"
line2 = "2 99999 053.2500 289.2473 0002259 270.0071 269.9405 02.00530899000014"
satellite3 = EarthSatellite(line1, line2, "Satellite3", ts)

line1 = "1 99999U          25001.16666667 -.00000054  00000-0 -17968+3 0 00004"
line2 = "2 99999 053.1960 334.1974 0002254 269.7698 180.2350 02.00532303000010"
satellite4 = EarthSatellite(line1, line2, "Satellite4", ts)

line1 = "1 99999U          25001.16666667  .00000026  00000-0  86103+2 0 00007"
line2 = "2 99999 053.1017 019.1717 0002246 269.8680 090.1894 02.00531431000014"
satellite5 = EarthSatellite(line1, line2, "Satellite5", ts)

line1 = "1 99999U          25001.16666667  .00000009  00000-0  31017+2 0 00002"
line2 = "2 99999 052.9931 064.2078 0002240 269.9267 000.0648 02.00530999000018"
satellite6 = EarthSatellite(line1, line2, "Satellite6", ts)

line1 = "1 99999U          25001.16666667 -.00000151  00000-0 -50067+3 0 00009"
line2 = "2 99999 052.9761 109.3038 0002248 270.0550 269.7345 02.00532234000014"
satellite7 = EarthSatellite(line1, line2, "Satellite7", ts)

line1 = "1 99999U          25001.16666667 -.00000152  00000-0 -50490+3 0 00000"
line2 = "2 99999 053.0739 154.3740 0002243 269.8755 179.7821 02.00532195000014"
satellite8 = EarthSatellite(line1, line2, "Satellite8", ts)

satellites = [
    satellite1,
    satellite2,
    satellite3,
    satellite4,
    satellite5,
    satellite6,
    satellite7,
    satellite8,
]

# 创建 100 个地面站（随机分布在全球）
num_ground_stations = 1
ground_stations = [
    Topos(
        # latitude_degrees=np.random.uniform(-90, 90),
        # longitude_degrees=np.random.uniform(-180, 180),
        latitude_degrees=36,
        longitude_degrees=108,
    )
    for _ in range(num_ground_stations)
]

# 设置时间范围为 2024年12月4日 04:00:00 到 2024年12月5日 04:00:00
start_time = datetime(2025, 1, 1, 4, 0, 0)
end_time = datetime(2025, 1, 1, 16, 0, 0)
xx = int((end_time - start_time).total_seconds())

time_step = 10  # 每隔 10 s计算一次
times = ts.utc(
    start_time.year,
    start_time.month,
    start_time.day,
    start_time.hour,
    start_time.minute,
    np.arange(0, int((end_time - start_time).total_seconds()) + time_step, time_step),
)

x = times[0].utc_iso()
y = times[-1].utc_iso()


# 计算可见性并记录可见时段
def get_altitude(satellite, ground_station, times):
    # 计算卫星与地面站之间的角度
    altitudes_data = []
    difference = satellite - ground_station
    topocentric = difference.at(times)
    altitudes_data = topocentric.altaz()[0].degrees
    # print(altitudes_data)

    latitudes = satellite.at(times).subpoint().latitude.degrees
    in_lat_zone = (latitudes >= -15) & (latitudes <= 15)

    # for i, bool in enumerate(in_lat_zone):
    # if bool:
    # altitudes_data[i] = 0

    # print(altitudes_data)

    return altitudes_data


start = time.time()
for station in ground_stations:
    altitude_data = []
    for satellite in satellites:
        altitude_data.append(get_altitude(satellite, station, times))
    # 找出每列（n组）的最大值及其索引
    max_values = np.max(altitude_data, axis=0)  # 每列最大值
    max_indices = np.argmax(altitude_data, axis=0)  # 每列最大值的行索引

    index_now = max_indices[0]
    for i, index in enumerate(max_indices):
        if index != index_now:
            print(f"from {index_now} to {index}:")
            index_now = index
            time_handover = ts.utc(
                start_time.year,
                start_time.month,
                start_time.day,
                start_time.hour,
                start_time.minute,
                time_step * i,
            )
            print(time_handover.utc_iso())

end = time.time()
# 计算耗时
elapsed_time = end - start
print(f"运行耗时: {elapsed_time:.6f} 秒")
print(altitude_data)

# 找出每列（n组）的最大值及其索引
max_values = np.max(altitude_data, axis=0)  # 每列最大值
max_indices = np.argmax(altitude_data, axis=0)  # 每列最大值的行索引

# 打印结果
for i, (value, index) in enumerate(zip(max_values, max_indices)):
    print(f"Group {i + 1}: Max Value = {value}, Index = {index}")

index_now = max_indices[0]
for i, index in enumerate(max_indices):
    if index != index_now:
        print(f"from {index_now} to {index}:")
        index_now = index
        time_handover = ts.utc(
            start_time.year,
            start_time.month,
            start_time.day,
            start_time.hour,
            start_time.minute,
            time_step * i,
        )
        print(time_handover.utc_iso())
