import numpy as np
import queue
from datetime import datetime, timedelta
import networkx as nx
from sklearn.preprocessing import StandardScaler
from skyfield.api import Topos
from skyfield.framelib import itrs


from create_calculate_scenario import set_calculate_time

def get_visible_intervals(satellite, ground_station, times):
    # ================== 修正后的坐标计算 ==================
    # 获取地面站ITRF坐标（转换为列向量）
    t0 = times[0]
    gs_pos = ground_station.at(t0).frame_xyz(itrs).km.reshape(3, 1)  # 形状(3,1)

    # 计算目标卫星ITRF坐标（保持时间在第二维）
    sat_pos = satellite.at(times).frame_xyz(itrs).km  # 形状(3, N)

    # 计算目标向量（地面站->卫星的方向向量）
    target_vectors = sat_pos - gs_pos  # 广播后形状(3, N)
    target_vectors = target_vectors.T  # 转换为(N, 3)以便后续矩阵运算

    # 生成GEO卫星向量（同样转换为列向量）
    geo_vectors = []
    for lon in range(0, 360, 5):
        geo = Topos(longitude_degrees=lon, latitude_degrees=0, elevation_m=35786e3)
        geo_pos = geo.at(t0).frame_xyz(itrs).km.reshape(3, 1)
        geo_vectors.append(geo_pos - gs_pos)  # 形状(3,1)
    geo_vectors = np.hstack(geo_vectors).T  # 最终形状(72,3)

    # ================== 修正后的夹角计算 ==================
    # 使用矩阵乘法进行批量计算
    dot_products = target_vectors @ geo_vectors.T  # 形状(N,72)
    norm_target = np.linalg.norm(target_vectors, axis=1, keepdims=True)
    norm_geo = np.linalg.norm(geo_vectors, axis=1, keepdims=True).T
    cos_theta = dot_products / (norm_target * norm_geo)
    geo_conflict = np.any(cos_theta >= np.cos(np.radians(5)), axis=1)

    # 计算卫星与地面站之间的角度
    difference = satellite - ground_station
    topocentric = difference.at(times)
    altitudes = topocentric.altaz()[0].degrees

    latitudes = satellite.at(times).subpoint().latitude.degrees
    in_lat_zone = (latitudes >= -15) & (latitudes <= 15)

    # 计算卫星视场角约束
    sat_positions = satellite.at(times)
    gs_positions = ground_station.at(times)

    # 卫星到地面站的向量（地固坐标系）
    sat_to_gs_vectors = (
        gs_positions.position.km - sat_positions.position.km
    ).T  # Shape (N, 3)

    # 卫星天顶方向（指向地心）
    sat_zenith_vectors = (-sat_positions.position.km).T  # Shape (N, 3)
    # 计算点积和模长
    dot_products = np.sum(sat_to_gs_vectors * sat_zenith_vectors, axis=1)
    norm_sat_to_gs = np.linalg.norm(sat_to_gs_vectors, axis=1)
    norm_zenith = np.linalg.norm(sat_zenith_vectors, axis=1)

    # 计算夹角并判断视场条件
    cos_theta = dot_products / (norm_sat_to_gs * norm_zenith)
    cos_theta = np.clip(cos_theta, -1.0, 1.0)  # 处理计算误差
    theta = np.degrees(np.arccos(cos_theta))
    in_fov = theta <= 13.6

    visible = (
        (altitudes > 0) & in_fov & (~geo_conflict)
    )  # (~in_lat_zone) & in_fov  # 可见时刻标识符


    # 提取可见时段
    visible_intervals = []
    current_start = None
    current_alts = []

    for i in range(len(times)):
        if visible[i]:
            if current_start is None:
                current_start = times[i]
            current_alts.append(altitudes[i])  # 收集当前时段的仰角
        else:
            if current_start is not None:
                # 计算平均仰角
                avg_alt = sum(current_alts) / len(current_alts)
                visible_intervals.append(
                    (current_start.utc_iso(), times[i - 1].utc_iso(), avg_alt)
                )
                current_start = None
                current_alts = []

    # 处理最后一个可见时段
    if current_start is not None:
        avg_alt = sum(current_alts) / len(current_alts)
        visible_intervals.append(
            (current_start.utc_iso(), times[-1].utc_iso(), avg_alt)
        )

    return visible_intervals

# 根据可见时段构建切换有向图，无权重（切换次数最少）
def generate_map(intervals,start_time,end_time):
    '''
    根据卫星与用户可见关系，生成计算周期内的切换判决有向图，有向图结点表示用户接入某颗卫星，边表示一次切换

    输入：
        卫星-用户可见时段，计算周期起始时刻，结束时刻

    输出：
        有向图的邻接矩阵
    '''
    q = queue.Queue()
    processFlag = np.zeros(len(intervals))
    A = np.zeros((len(intervals) + 2, len(intervals) + 2))
    for interval in intervals:
        if interval[2] == start_time:
            q.put(interval[0])
            A[0][interval[0]] = 1
        if interval[3] > end_time + timedelta(minutes=5):  
            # 可见时段结束时刻需要比计算时段结束时刻延后5分钟，防止下一个计算周期起始节点为该可见时段，而下个周期该可见时段过短无法完成切换
            A[interval[0]][len(intervals) + 1] = 1

    while q.empty() == 0:
        num = q.get()
        interval_now = intervals[num - 1]
        for interval in intervals:
            if interval[2] < interval_now[3] and interval[3] > interval_now[3]:
                A[interval_now[0]][interval[0]] = 1
                if processFlag[interval[0] - 1] == 0:
                    q.put(interval[0])
            elif interval[2] > interval_now[2] and interval[2] < interval_now[3]:
                A[interval_now[0]][interval[0]] = 1
                if processFlag[interval[0] - 1] == 0:
                    q.put(interval[0])
        processFlag[num - 1] = 1

    return A

def sigmoid(x):
    y = 1/(1+np.exp(-x))
    return y

###
# server:用户当前的服务卫星，用于确定计算切换策略的起始节点
###
def generate_handover_strategy(startTime,stopTime,server,position,satellites,satellite_load_num):
    '''
    生成用户的切换策略

    输入：
        计算周期起始时刻，结束时刻
        用户当前的服务卫星，用于确定切换判决起始结点
        用户位置
        卫星对象，卫星负载情况（由于这两个变量在main.py中创建并维护，这里作为函数输入）

    输出：
        在当前计算周期内，用户的切换策略列表，列表中每项包括切换前后服务卫星编号，切换时间窗口
    '''
    time_step = 10
    times = set_calculate_time(startTime,stopTime+timedelta(minutes=10),time_step) # 可见时段计算重叠10分钟作为保护间隔

    visibility_results = []
    for j, satellite in enumerate(satellites):
        visible_intervals = get_visible_intervals(satellite, position, times)
        if visible_intervals:
            for interval in visible_intervals:
                visibility_results.append((j + 1, interval))
    intervals = []
    k = 1
    for interval in visibility_results:
        time1 = datetime.fromisoformat(interval[1][0].replace("Z", ""))
        time2 = datetime.fromisoformat(interval[1][1].replace("Z", ""))
        intervals.append((k, interval[0], time1, time2,interval[-1][-1]))
        k = k + 1
    
    # 获得权重数据，归一化
    weight_altitude = []
    weight_duration = []
    weight_load = []
    for interval in intervals:
        weight_altitude.append(interval[-1])
        weight_duration.append((interval[3]-interval[2]).total_seconds())
        weight_load.append(satellite_load_num[interval[1]])
    
    scaler2 = StandardScaler()
    weight_altitude = sigmoid(scaler2.fit_transform(np.array(weight_altitude).reshape(-1, 1)).flatten())
    weight_duration = sigmoid(scaler2.fit_transform(np.array(weight_duration).reshape(-1, 1)).flatten())
    weight_load= sigmoid(scaler2.fit_transform(np.array(weight_load).reshape(-1, 1)).flatten())
    
    A = generate_map(intervals,startTime,stopTime)
    # 创建有向图
    G = nx.DiGraph()
    # G.clear()

    # 添加节点
    for i in range(len(A)):
        G.add_node(i)

        # 添加有向边，根据邻接矩阵
    for i in range(len(A)):
        for j in range(len(A)):
            if A[i, j] == 1:
                #无权
                #G.add_edge(i, j)

                #带权
                if j != k:
                    # 仰角+服务时间长度+负载
                    weight = weight_load[j-1] + (1-weight_altitude[j-1]) + (1- weight_duration[j-1])
                    G.add_edge(i, j, weight=weight)
                else:
                    G.add_edge(i, j)
                    
    shortest_path = nx.dijkstra_path(G, source=0, target=len(A) - 1)
    for interval in intervals:
        if interval[2] == startTime and interval[1] == server:
            shortest_path = nx.dijkstra_path(G, source=interval[0], target=len(A) - 1)

    
    # 获得切换窗口信息
    num = len(shortest_path)
    result = []
    for i in range(num-2):
        id1 = shortest_path[i]
        id2 = shortest_path[i+1]
        if id1 == 0:
            start_interval = startTime
            end_interval = intervals[id2 - 1][2]
            said_now = 0
            said = intervals[id2 - 1][1]
        elif id2 !=k:
            start_interval = intervals[id2 - 1][2]
            end_interval = intervals[id1 - 1][3] if intervals [id1 - 1][3] <= stopTime else stopTime# 计算可见性留有10分钟保护间隔后，还需要保证上一个计算周期内的切换窗口在计算时段内
            said_now = intervals[id1 - 1][1]
            said = intervals[id2 - 1][1]

        result.append(
            [said_now,said,start_interval,end_interval]
        )

    return result