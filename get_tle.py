import win32com.client

# 启动STK应用程序
stkApp = win32com.client.Dispatch("STK11.Application")  # STK版本根据你的版本调整
stkApp.Visible = True  # 设置STK应用程序为可见

# 获取STK根对象
root = stkApp.Personality2

# 打开已存在的场景
# scenario_path = "C:\\Users\\DELL\\Documents\\STK 11 (x64)\\Scenario3"
scenario = root.CurrentScenario  # (scenario_path)  # 加载场景

# 获取场景中的卫星对象
satellite_name = "Satellite111_SGP4"  # 假设你的卫星对象名为Satellite1
satellite = scenario.Children.Item(satellite_name)  # 根据卫星名称获取卫星对象

# 获取卫星的TLE数据
tle_provider = satellite.DataProviders.Item("Element Set")
tle_data = tle_provider.Exec()
tle_line1 = tle_data.DataSets.GetDataSetByName("Two Line").GetValues()
# tle_line2 = tle_data.DataSets.GetDataSetByName("Line2").GetValues()


# 打印TLE数据
print("TLE Line 1:", tle_line1[0])
print("TLE Line 2:", tle_line1[1])

# 关闭STK应用（如果需要）
# stkApp.Quit()
