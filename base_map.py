import queue
import time
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from skyfield.api import EarthSatellite, Topos, load



class UE:
    def __init__(self,position,time):
        self.position = position
        self.validTime = time

# 加载天文常数
ts = load.timescale()


def set_satellites():
    # TSN八星 时刻2025 3 3 4 0 0

    line1 = "1 99999U          25062.16666667  .00000212  00000-0  70351+3 0 00000"
    line2 = "2 99999 053.2929 259.2235 0002254 270.1262 089.8058 02.00530785000017"
    satellite1 = EarthSatellite(line1, line2, "Satellite1", ts)

    line1 = "1 99999U          25062.16666667  .00000090  00000-0  29748+3 0 00008"
    line2 = "2 99999 053.2513 304.1327 0002257 270.0865 000.0248 02.00530768000016"
    satellite2 = EarthSatellite(line1, line2, "Satellite2", ts)

    line1 = "1 99999U          25062.16666667 -.00000050  00000-0 -16662+3 0 00007"
    line2 = "2 99999 053.1477 349.1079 0002258 269.9243 270.2277 02.00531954000016"
    satellite3 = EarthSatellite(line1, line2, "Satellite3", ts)

    line1 = "1 99999U          25062.16666667  .00000068  00000-0  22531+3 0 00006"
    line2 = "2 99999 053.0596 034.1385 0002252 269.8744 180.2281 02.00531552000016"
    satellite4 = EarthSatellite(line1, line2, "Satellite4", ts)

    line1 = "1 99999U          25062.16666667  .00000125  00000-0  41588+3 0 00003"
    line2 = "2 99999 053.0144 079.1874 0002244 270.0623 089.9818 02.00530074000017"
    satellite5 = EarthSatellite(line1, line2, "Satellite5", ts)

    line1 = "1 99999U          25062.16666667 -.00000130  00000-0 -43203+3 0 00007"
    line2 = "2 99999 053.0035 124.2510 0002249 270.0378 359.9027 02.00529921000018"
    satellite6 = EarthSatellite(line1, line2, "Satellite6", ts)

    line1 = "1 99999U          25062.16666667 -.00000275  00000-0 -91381+3 0 00007"
    line2 = "2 99999 053.0758 169.3115 0002253 269.9216 269.8766 02.00531467000011"
    satellite7 = EarthSatellite(line1, line2, "Satellite7", ts)

    line1 = "1 99999U          25062.16666667 -.00000029  00000-0 -96095+2 0 00000"
    line2 = "2 99999 053.2058 214.3077 0002255 269.9599 179.8132 02.00531710000018"
    satellite8 = EarthSatellite(line1, line2, "Satellite8", ts)

    satellites = [
        satellite1,
        satellite2,
        satellite3,
        satellite4,
        satellite5,
        satellite6,
        satellite7,
        satellite8,
    ]

    return satellites


# 1. set satellites
satellites = set_satellites()


# 2. set facility
def set_facilitys(latitude, longitude):
    ground_station = Topos(
        latitude_degrees=latitude,
        longitude_degrees=longitude,
    )
    return ground_station


# 创建 100 个地面站（随机分布在全球）
num_ground_stations = 1
latitude = -69.0390
longtitude = -105.4776
ground_stations = [
    set_facilitys(latitude, longtitude) for _ in range(num_ground_stations)
]

# 尝试创建UE对象
ue = UE(set_facilitys(latitude, longtitude),datetime.now())


# 3. set_calculate_time
def set_calculate_time(start_time, end_time, time_step):
    times = ts.utc(
        start_time.year,
        start_time.month,
        start_time.day,
        start_time.hour,
        start_time.minute,
        np.arange(
            0, int((end_time - start_time).total_seconds()) + time_step, time_step
        ),
    )
    return times


start_time = datetime(2025, 3, 3, 15, 0, 0)
end_time = datetime(2025, 3, 4, 15, 0, 0)
time_step = 10  # 每隔 10 s计算一次
times = set_calculate_time(start_time, end_time, time_step)


# 4. 计算可见性并记录可见时段
def get_visible_intervals(satellite, ground_station, times):
    # 计算卫星与地面站之间的角度
    difference = satellite - ground_station
    topocentric = difference.at(times)
    altitudes = topocentric.altaz()[0].degrees
    azimuth = topocentric.altaz()[1].degrees

    latitudes = satellite.at(times).subpoint().latitude.degrees
    in_lat_zone = (latitudes >= -15) & (latitudes <= 15)

    visible = (altitudes > 0) & (~in_lat_zone)  # 可见时刻标识符

    # 提取可见时段
    visible_intervals = []
    start_time = None

    for i in range(1, len(visible)):
        if i == 1 and visible[i - 1]:
            start_time = times[0]
        elif visible[i] and not visible[i - 1]:
            # 可见时段的开始
            start_time = times[i]
        elif not visible[i] and visible[i - 1] and start_time is not None:
            # 可见时段的结束
            visible_intervals.append((start_time.utc_iso(), times[i].utc_iso()))
            start_time = None
        elif i == len(visible) - 1 and visible[i]:
            visible_intervals.append((start_time.utc_iso(), times[i].utc_iso()))
            start_time = None

    return visible_intervals


def get_visible_intervals_2(satellite, ground_station, times):
    # 计算卫星与地面站之间的角度
    difference = satellite - ground_station
    topocentric = difference.at(times)
    altitudes = topocentric.altaz()[0].degrees
    azimuth = topocentric.altaz()[1].degrees

    latitudes = satellite.at(times).subpoint().latitude.degrees
    in_lat_zone = (latitudes >= -15) & (latitudes <= 15)

    visible = (altitudes > 0) & (~in_lat_zone)  # 可见时刻标识符

    # 提取可见时段
    visible_intervals = []
    current_start = None
    current_alts = []

    for i in range(len(times)):
        if visible[i]:
            if current_start is None:
                current_start = times[i]
            current_alts.append(altitudes[i])  # 收集当前时段的仰角
        else:
            if current_start is not None:
                # 计算平均仰角
                avg_alt = sum(current_alts) / len(current_alts)
                visible_intervals.append((
                    current_start.utc_iso(),
                    times[i-1].utc_iso(),
                    avg_alt
                ))
                current_start = None
                current_alts = []
    
    # 处理最后一个可见时段
    if current_start is not None:
        avg_alt = sum(current_alts) / len(current_alts)
        visible_intervals.append((
            current_start.utc_iso(),
            times[-1].utc_iso(),
            avg_alt
        ))
    
    return visible_intervals


# 根据可见时段构建切换有向图，无权重（切换次数最少）
def generate_map(intervals):
    q = queue.Queue()
    print(len(intervals))
    processFlag = np.zeros(len(intervals))
    A = np.zeros((len(intervals) + 2, len(intervals) + 2))
    for interval in intervals:
        if interval[2] == start_time:
            q.put(interval[0])
            A[0][interval[0]] = 1
            # print(A)
        if interval[3] == end_time:
            A[interval[0]][len(intervals) + 1] = 1
    print(list(q.queue))

    while q.empty() == 0:
        num = q.get()
        interval_now = intervals[num - 1]
        for interval in intervals:
            if interval[2] < interval_now[3] and interval[3] > interval_now[3]:
                A[interval_now[0]][interval[0]] = 1
                if processFlag[interval[0] - 1] == 0:
                    q.put(interval[0])
            # processFlag[interval[0]] = 1
            elif interval[2] > interval_now[2] and interval[2] < interval_now[3]:
                A[interval_now[0]][interval[0]] = 1
                if processFlag[interval[0] - 1] == 0:
                    q.put(interval[0])
                # processFlag[interval[0]] = 1
        processFlag[num - 1] = 1

    return A

def sigmoid(x):
    y = 1/(1+np.exp(-x))
    return y

# 记录每个地面站的可见时段，计算有向图、最短路径
visibility_results = []
start = time.time()
for i, station in enumerate(ground_stations):
    visibility_results = []
    for j, satellite in enumerate(satellites):
        visible_intervals = get_visible_intervals_2(satellite, station, times)
        # visibility_results[f"Station_{i + 1} Satellite_{j + 1}"] = visible_intervals
        if visible_intervals:
            for interval in visible_intervals:
                visibility_results.append((j + 1, interval))

    # 转换可见时段格式
    intervals = []
    k = 1
    for interval in visibility_results:
        #print(interval)
        time1 = datetime.fromisoformat(interval[1][0].replace("Z", ""))
        time2 = datetime.fromisoformat(interval[1][1].replace("Z", ""))
        intervals.append((k, interval[0], time1, time2,interval[-1][-1]))
        k = k + 1
    
    # 获得权重数据，归一化
    weight_altitude = []
    weight_duration = []
    for interval in intervals:
        print(interval,interval[3]-interval[2],(interval[3]-interval[2]).total_seconds())
        weight_altitude.append(interval[-1])
        weight_duration.append((interval[3]-interval[2]).total_seconds())

    scaler = MinMaxScaler()
    scaler2 = StandardScaler()
    weight_altitude_normalized = scaler2.fit_transform(np.array(weight_altitude).reshape(-1, 1)).flatten()
    weight_duration_normalized = scaler2.fit_transform(np.array(weight_duration).reshape(-1, 1)).flatten()

    weight_altitude_normalized_sigmoid = sigmoid(weight_altitude_normalized)
    weight_duration_normalized_sigmoid = sigmoid(weight_duration_normalized)

    A = generate_map(intervals)  # 邻接矩阵
    # 创建有向图
    G = nx.DiGraph()
    # G.clear()

    # 添加节点
    for i in range(len(A)):
        G.add_node(i)

        # 添加有向边，根据邻接矩阵
    for i in range(len(A)):
        for j in range(len(A)):
            if A[i, j] == 1:
                #G.add_edge(i, j,)

                #带权
                
                if j != k:
                    weight = 1 / weight_altitude_normalized_sigmoid[j-1] #+ 0.5 / weight_duration_normalized_sigmoid[j-1]
                    G.add_edge(i, j, weight = weight)
                else:
                    G.add_edge(i, j)
    # 绘制图形
    plt.figure(figsize=(8, 6))
    pos = nx.spring_layout(G)
    nx.draw(
        G,
        pos,
        with_labels=True,
        node_size=2000,
        node_color="skyblue",
        font_size=16,
        font_weight="bold",
        arrows=True,
    )
    labels = nx.get_edge_attributes(G,'weight')
    nx.draw_networkx_edge_labels(G,pos,edge_labels=labels)
    plt.title("Directed Graph from Adjacency Matrix")
    plt.show()
    if nx.dijkstra_path(G, source=0, target=len(A) - 1):
        shortest_path = nx.dijkstra_path(G, source=0, target=len(A) - 1)
        shortest_distance = nx.dijkstra_path_length(G, source=0, target=len(A) - 1)
        print(f"###最短路径: {shortest_path}")
    else:
        print("无最短路径")

    num = len(shortest_path)
    # for index in shortest_path:
    #    if index != 0 and index != k:
    #       print(f"卫星编号：{intervals[index - 1][1]}，切换时机：")
    print(f"时间段{start_time}-{end_time}内切换策略:")
    print(f"用户经纬度：{latitude},{longtitude}")
    id_now = 0
    for i in range(num):
        id = shortest_path[i]
        if id != 0 and id != k:
            start_interval = intervals[id - 1][2]

        if id_now != 0 and id != k:
            end_interval = intervals[id_now - 1][3]
            said = intervals[id - 1][1]
            said_now = intervals[id_now - 1][1]
            print(
                f"原卫星编号：{said_now}，目的卫星编号：{said}，切换窗口：{start_interval},{end_interval}"
            )

        id_now = id


end = time.time()
# 计算耗时
elapsed_time = end - start
print(f"运行耗时: {elapsed_time:.6f} 秒")


# 转换可见时段
intervals = []
k = 1
for interval in visibility_results:
    time1 = datetime.fromisoformat(interval[1][0].replace("Z", ""))
    time2 = datetime.fromisoformat(interval[1][1].replace("Z", ""))
    intervals.append((k, interval[0], time1, time2))
    k = k + 1

for interval in intervals:
    print(interval)


# for station, intervals in visibility_results.items():
#    print(f"{station}:")
#   if intervals:
#        for interval in intervals:
#            print(f"  - {interval[0]} to {interval[1]}")
#    else:
#        print("  No visibility.")


A = generate_map(intervals)  # 邻接矩阵
print(A)
# 创建有向图
G = nx.DiGraph()

# 添加节点
for i in range(len(A)):
    G.add_node(i)

# 添加有向边，根据邻接矩阵
for i in range(len(A)):
    for j in range(len(A)):
        if A[i, j] == 1:
            G.add_edge(i, j)

# 绘制图形
plt.figure(figsize=(8, 6))
nx.draw(
    G,
    with_labels=True,
    node_size=2000,
    node_color="skyblue",
    font_size=16,
    font_weight="bold",
    arrows=True,
)
plt.title("Directed Graph from Adjacency Matrix")
plt.show()


shortest_path = nx.dijkstra_path(G, source=0, target=len(A) - 1)
shortest_distance = nx.dijkstra_path_length(G, source=0, target=len(A) - 1)

print(f"最短路径: {shortest_path}")
print(f"最短路径长度: {shortest_distance}")


num = len(shortest_path)
# for index in shortest_path:
#    if index != 0 and index != k:
#       print(f"卫星编号：{intervals[index - 1][1]}，切换时机：")
print(f"时间段{start_time}-{end_time}内切换策略:")
print(f"用户经纬度：{latitude},{longtitude}")
id_now = 0
for i in range(num):
    id = shortest_path[i]
    if id != 0 and id != k:
        start_interval = intervals[id - 1][2]

    if id_now != 0 and id != k:
        end_interval = intervals[id_now - 1][3]
        said = intervals[id - 1][1]
        said_now = intervals[id_now - 1][1]
        print(
            f"原卫星编号：{said_now}，目的卫星编号：{said}，切换窗口：{start_interval},{end_interval}"
        )

    id_now = id


end = time.time()
# 计算耗时
elapsed_time = end - start
print(f"运行耗时: {elapsed_time:.6f} 秒")
