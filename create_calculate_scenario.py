from skyfield.api import Topos,load
import numpy as np


# 加载天文常数
ts = load.timescale()

def set_facilitys(latitude, longitude):
    '''
    创建地面用户对象

    输入：
        经纬度
        
    输出：
        Topos对象，表示某个经纬度位置上的用户
    '''
    ground_station = Topos(
        latitude_degrees=latitude,
        longitude_degrees=longitude,
    )
    return ground_station

def set_calculate_time(start_time,end_time,time_step):
    '''
    设置计算可见性的时段

    输入:
        计算起始时刻，计算结束时刻，时间步长

    输出：
        计算可见性的时刻序列
    '''
    num = int((end_time - start_time).total_seconds())
    times = ts.utc(
        start_time.year,
        start_time.month,
        start_time.day,
        start_time.hour,
        start_time.minute,
        start_time.second + np.arange(
            0, (num+time_step), time_step
        ),
    )
    return times