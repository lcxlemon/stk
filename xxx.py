import queue
import time
from datetime import datetime, timedelta

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from skyfield.api import EarthSatellite, Topos, load
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler

import threading
import sys
A = np.array([[0., 1., 1., 1., 0., 0.],
       [0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 1., 1.],
       [0., 0., 1., 0., 1., 0.],
       [0., 0., 1., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0.]])

# 创建有向图
G = nx.DiGraph()
# G.clear()

# 添加节点
for i in range(len(A)):
    G.add_node(i)

    # 添加有向边，根据邻接矩阵
for i in range(len(A)):
    for j in range(len(A)):
        if A[i, j] == 1:
            #无权
            G.add_edge(i, j,weight = 0)

            #带权
            # if j != k:
            #     # 仰角+服务时间长度+负载
            #     weight = weight_load[j-1] + (1-weight_altitude[j-1]) + (1- weight_duration[j-1])
            #     G.add_edge(i, j, weight=weight)
            # else:
            #     G.add_edge(i, j)
# 绘制图形
plt.figure(figsize=(8, 6))
pos = nx.spring_layout(G)
nx.draw(
    G,
    pos,
    with_labels=True,
    node_size=2000,
    node_color="skyblue",
    font_size=16,
    font_weight="bold",
    arrows=True,
)
labels = nx.get_edge_attributes(G,'weight')
nx.draw_networkx_edge_labels(G,pos,edge_labels=labels)
plt.title("Directed Graph from Adjacency Matrix")
plt.show()
ortest_path = nx.dijkstra_path(G, source=0, target=len(A) - 1)
print(ortest_path)
