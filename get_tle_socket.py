import socket
import time

# 连接到STK Remote Interface的IP和端口
host = "192.168.1.26"  # 远程STK机器的IP地址
port = 5001  # STK远程接口的端口，默认为5001

# 创建socket连接
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((host, port))

command = 'GenerateTLE */Satellite/Satellite111 Sampling "1 Jun 2025 4:00:00.00" "11 Jun 2025 4:00:00.00" 300.0 "1 Jun 2025 12:00:00.00" 55443 20 0.0001 SGP4 Satellite111_SGP4\n'
client_socket.sendall(command.encode("utf-8"))


# 向STK发送命令
command = ' Report_RM */Satellite/Satellite111 Style "TLE"\n'  # 这是假设的命令，具体命令取决于STK的接口规范
# command = 'Report_RM */Facility/Facility1 Style "Cartesian Position"\n'
client_socket.sendall(command.encode("utf-8"))

# 接收STK的响应\
response = client_socket.recv(4096)
time.sleep(2)
data = client_socket.recv(4096)


# response = b""
# while True:
#    data = client_socket.recv(4096)
#    if not data:
#        break
#    response += data

#    if b"END_OF_REPORT" in response:
#        break
# response = client_socket.recv(4096)
print(data.decode("utf-8"))

# 关闭连接
client_socket.close()
