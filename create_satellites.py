from skyfield.api import EarthSatellite, load

# 加载天文常数
ts = load.timescale()


def set_satellites():
    '''
    根据TLE文件内容创建卫星对象

    输入：
        无（后续改为读取TLE文件）
        
    输出：
        卫星对象组成的列表
    '''
    # TSN八星 时刻2025 3 3 4 0 0
    line1 = "1 99999U          25062.16666667  .00000212  00000-0  70351+3 0 00000"
    line2 = "2 99999 053.2929 259.2235 0002254 270.1262 089.8058 02.00530785000017"
    satellite1 = EarthSatellite(line1, line2, "Satellite1", ts)

    line1 = "1 99999U          25062.16666667  .00000090  00000-0  29748+3 0 00008"
    line2 = "2 99999 053.2513 304.1327 0002257 270.0865 000.0248 02.00530768000016"
    satellite2 = EarthSatellite(line1, line2, "Satellite2", ts)

    line1 = "1 99999U          25062.16666667 -.00000050  00000-0 -16662+3 0 00007"
    line2 = "2 99999 053.1477 349.1079 0002258 269.9243 270.2277 02.00531954000016"
    satellite3 = EarthSatellite(line1, line2, "Satellite3", ts)

    line1 = "1 99999U          25062.16666667  .00000068  00000-0  22531+3 0 00006"
    line2 = "2 99999 053.0596 034.1385 0002252 269.8744 180.2281 02.00531552000016"
    satellite4 = EarthSatellite(line1, line2, "Satellite4", ts)

    line1 = "1 99999U          25062.16666667  .00000125  00000-0  41588+3 0 00003"
    line2 = "2 99999 053.0144 079.1874 0002244 270.0623 089.9818 02.00530074000017"
    satellite5 = EarthSatellite(line1, line2, "Satellite5", ts)

    line1 = "1 99999U          25062.16666667 -.00000130  00000-0 -43203+3 0 00007"
    line2 = "2 99999 053.0035 124.2510 0002249 270.0378 359.9027 02.00529921000018"
    satellite6 = EarthSatellite(line1, line2, "Satellite6", ts)

    line1 = "1 99999U          25062.16666667 -.00000275  00000-0 -91381+3 0 00007"
    line2 = "2 99999 053.0758 169.3115 0002253 269.9216 269.8766 02.00531467000011"
    satellite7 = EarthSatellite(line1, line2, "Satellite7", ts)

    line1 = "1 99999U          25062.16666667 -.00000029  00000-0 -96095+2 0 00000"
    line2 = "2 99999 053.2058 214.3077 0002255 269.9599 179.8132 02.00531710000018"
    satellite8 = EarthSatellite(line1, line2, "Satellite8", ts)

    satellites = [
        satellite1,
        satellite2,
        satellite3,
        satellite4,
        satellite5,
        satellite6,
        satellite7,
        satellite8,
    ]

    return satellites